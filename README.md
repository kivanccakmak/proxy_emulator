#proxy_emulator
------------
This repository is created to emulate proxy server on **_Python_**.
Experiments should be done with 3 PC's. 

Here, parantheses are used for ID of PC.
I assume that first PC acts as data generator(transmitter), second PC acts as data receiver
and third PC acts as proxy. 

```
(1) python slave_controller.py <3. PC's IP ADDRESS> <PORT NUMBER 1> <transmitter>
(2) python slave_controller.py <OWN IP ADDRESS> <PORT NUMBER 2> <receiver>
(3) python master_controller.py <First PC's IP ADDRESS> <PORT NUMBER 1> <Second PC's IP ADDRESS> <PORT NUMBER 2> <SOCKET QUANTITY>
```

With respect to last command variable (which is **_socket quantity_**) of **_master_controller.py_**, proxy PC opens multiple parallel 
TCP connections towards receiver PC -here I used **_subprocess_** module of python. 

In addition, proxy PC records amount of data received from transmitter PC on **_redis server_**. Consequently, proxy PC checks whether
data is available on buffer. If it is, proxy PC starts transmisson; otherwise it delays it's process, then checks buffer again...

If available buffer is higher than **_upper threshold_**, proxy PC sets a delay before transmitting TCP acknowledgements towards
transmitter PC. By doing so, link rate in between transmitter - proxy PC's are decreased. 

On the other hand, if buffer is less than **_lower threshold_**, proxy PC decreases -if there is- delay value in between transmitter-proxy
link.             

 

  
