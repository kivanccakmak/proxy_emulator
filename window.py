#usr/bin/env/python
import os

def window_record():
    os.system('modprobe -r tcp_probe')
    os.system('tcp_probe [port=] full=1')
    os.system('chmod 444 /proc/net/tcpprobe')
    dir_name = os.getcwd()
    os.system('/proc/net/tcpprobe > {}/{} '.format(dir_name, 'window_size.out'))
   
def main():
    window_record()
    

if __name__ == "__main__":
    main()
