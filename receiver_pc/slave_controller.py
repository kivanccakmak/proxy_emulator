#!/usr/bin/env python
from task_finders import *
import dicts
import init
import socket
import sys
import os
import time
import timeit
import subprocess
duration = init.time['duration']
BUFFER_SIZE = 1024  # Normally 1024, but we want fast response
    
def slave_controller(TCP_IP, TCP_PORT, file_name, dir_name):
    file = open(os.path.join(dir_name, file_name), 'a')
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((TCP_IP, TCP_PORT))
    s.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
    s.listen(1); print("slave waits connection from IP: {} , PORT {}".format(TCP_IP, TCP_PORT)) 
    conn, addr = s.accept(); print 'Connection address:', addr
    tic = timeit.default_timer()
    while( float(timeit.default_timer() - tic) < duration):
        try:
            data = conn.recv(BUFFER_SIZE)
            if data:
                print "received data:" + data + "\n"
                file.write(data+ " \n ")
                list = data.split()
                port = list[dicts.req['port']]
                if list[dicts.req['order']] == 'open':
                    task = subprocess.Popen(["python", "receiver.py", TCP_IP, port, dir_name])
                    conn.send('subprocess.Popen(["python", "receiver.py", {}, {}, {}])'.format(TCP_IP,port,dir_name) )
                elif list[dicts.req['order']] == 'transmit2':
                    task = subprocess.Popen(["python", "transmitter.py", addr[0], port, dir_name])
                    conn.send('subprocess.Popen(["python", "transmitter.py", {}, {}, {}])'.format(TCP_IP,port,dir_name) )
                elif list[dicts.req['order']] == 'close':
                    conn.send('I will close \n'); time.sleep(0.1)
		    receive_task = TaskFinder('slave_task', 'receiver.py', 'receiver')
                    receive_task.get_list()
                    if receive_task.available:
                        pid = receive_task.get_pid(port)
                        os.system("kill {}".format(pid))
        except IndexError:
            conn.send('I dont understand Message\n'); time.sleep(0.1)
        except socket.error:
	    print("peer closed connection\n");
	    time.sleep(0.1); break
    conn.close()

def main():
    if len(sys.argv) != 4:
        print("Usage: {} <IP address> <PORT NUMBER> <type> ".format(sys.argv[0]))
        print("Example Usage: {} 127.0.0.1 5005 transmitter ".format(sys.argv[0]))
        sys.exit(1)
    if (sys.argv[3] != 'transmitter') & (sys.argv[3] != 'receiver'):
        print("Third variable has to be 'transmitter' or 'receiver' ")
        sys.exit(1)
    file_name = "slave_"+sys.argv[2]+"_controller"
    dir_name = sys.argv[3]
    try:
        os.mkdir(dir_name)
        slave_controller(sys.argv[1], int(sys.argv[2]), file_name, dir_name)
    except OSError:
        slave_controller(sys.argv[1], int(sys.argv[2]), file_name, dir_name)
    
if __name__ == "__main__":
    main()
