import init
import socket
import sys
import os
import time
import timeit

duration = init.time['duration'] #seconds
data_size = init.data['size']

def receiver(TCP_IP, TCP_PORT, data_file_name, dir_name):
    buf = bytearray(data_size)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((TCP_IP, TCP_PORT))
    s.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
    s.listen(1)
    print("ready to receive from PORT: {} ".format(TCP_PORT ))
    conn, addr = s.accept()
    print("Accepted Connection {}".format(addr))
    tic = timeit.default_timer()
    while( float(timeit.default_timer() - tic) < duration):
        tot_recv_bytes = 0
        recv_bytes = 0
        while tot_recv_bytes < data_size:
            recv_bytes = conn.recv_into(buf)
            if (recv_bytes > 0):
                tot_recv_bytes += recv_bytes		
                data_file = open(os.path.join(dir_name, data_file_name), 'a')
                data_file.write("time: {} |".format(str(timeit.default_timer() - tic)[0:6])+ " " + 
                "received data: {}".format(recv_bytes) + " \n")
                data_file.close()
    conn.close()

def main():
    if len(sys.argv) != 4:
        print("Usage: {} <IP address> <PORT NUMBER> <type> ".format(sys.argv[0]))
        sys.exit(1)
    if (sys.argv[3] != 'transmitter') & (sys.argv[3] != 'receiver'):
        print("Third variable has to be 'transmitter' or 'receiver' ")
        sys.exit(1)
    TCP_IP = sys.argv[1]
    TCP_PORT = int(sys.argv[2])
    data_file_name = "receiver_"+sys.argv[2]
    dir_name = sys.argv[3]
    receiver(TCP_IP, TCP_PORT, data_file_name, dir_name)
    
if __name__ == "__main__":
    main()
