import errno
import sys
import socket

def tx_socket_initialize(TCP_IP, TCP_PORT):
    print("trying to connect socket ({}, {})".format(TCP_IP, TCP_PORT))
    TCP_PORT = int(TCP_PORT)
    try:
        tx = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tx.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)    
        tx.connect((TCP_IP, TCP_PORT))
        return tx
    except socket.error, v:
        errorcode = v[0]
        if errorcode == errno.ECONNREFUSED:
            print("*** connection refused ***")
            sys.exit(1)    

    