from socket_initialize import *
import socket
import sys
import os
import time
import timeit
import init
import errno

duration = init.time['duration'] #seconds
data_size = init.data['size'] 

def transmitter(TCP_IP, TCP_PORT, data_file_name, dir_name):
    s = tx_socket_initialize(TCP_IP, int(TCP_PORT))
    print("connection established")
    tic = timeit.default_timer()
    while( float(timeit.default_timer() - tic) < duration):
        try:
            root_transmit(s, tic, dir_name, data_file_name)
        except socket.error, v:
            errorcode = v[0]
            if errorcode == errno.ECONNREFUSED:
                print("*** connection refused ***")
            elif errorcode != errno.ECONNRESET:
                print("*** connection reseted ***")
            s.close()
            sys.exit(1)
    s.close()
    sys.exit()    
        
def root_transmit(s, tic, dir_name, data_file_name):
    data_file = open(os.path.join(dir_name, data_file_name), 'a')
    buf = memoryview(bytearray(data_size))
    tot_send_bytes = 0
    send_bytes = s.send(buf[tot_send_bytes:])
    tot_send_bytes += send_bytes
    time = str(timeit.default_timer() - tic)
    data_file.write("time: {} |".format(str(time)[0:6]) + " " + "transmitted data: {}".format(send_bytes) + " \n") 
    data_file.close()

def main():
    if len(sys.argv) != 4:
        print("Usage: {} <IP address> <PORT NUMBER> <type> ".format(sys.argv[0]))
        sys.exit(1)
    TCP_IP = sys.argv[1]
    TCP_PORT = sys.argv[2]
    data_file_name = "transmitter_"+sys.argv[2]
    dir_name = sys.argv[3]
    transmitter(TCP_IP, TCP_PORT, data_file_name, dir_name)
    
if __name__ == "__main__":
    main()    
