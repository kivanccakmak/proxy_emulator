import os
import dicts
import sys
class TaskFinder(object):
    def __init__(self, dir_name, keyword, id):
        self.keyword = keyword 
        #(receiver.py, transmitter.py, proxy_transmitter.py, proxy_receiver.py)
        self.dir_name = dir_name 
        #master_task(for proxy) / slave_task (for receiver or transmitter)
        self.id = id 
        #transmitter, receiver or proxy
        self.available = False
        self.list = []
        if( (dir_name == 'master_task') | (dir_name == 'slave_task') ):
            try:
                os.mkdir(self.dir_name)
            except OSError:
                pass
        else:
            print("<dir_name> has to be 'slave_task' or 'master_task'") 
            sys.exit(1)
                    
    def get_list(self):
        order = "ps aux | grep " + self.keyword + " | grep -v grep" + " > "\
         + self.dir_name + "/process.txt"
        try:
            os.system(order)
        except OSError:
            os.system(order)
        file = open(os.path.join(self.dir_name, "process.txt"), 'r')
        counter = 0
        while True:
            line = file.readline()
            if line != '':
                list = line.split()
                if(self.keyword in line) &\
                (list[dicts.ps['program']]=='python') & (self.id in line):
		    port = self.get_port(list)
                    pid = list[dicts.ps['pid']]
		    try:
		        self.available = True
                        self.list.append([pid, port])
		    except IndexError:
			print("INDEX ERROR OCCURED")
			break
            elif (line == '') & (self.available == True):
                break
            else:
                break

    def get_port(self, list):
        if(self.id == 'proxy'):
            port = list[len(list)-1]
        else:
            port = list[len(list)-2]
        return port
    
        
    def get_pid(self, port):
        if self.available:
            for i in 0,len(self.list)-1:
                if self.list[i][dicts.task['port']] == port:
                    return self.list[i][dicts.task['pid']]
        else:
            return 0
                        
                
                
        
