import dicts
class Request():
    def __init__(self, TCP_CONTROL_IP, TCP_CONTROL_PORT, TCP_USE_PORT):
        self.CONTROL_IP = TCP_CONTROL_IP
        self.CONTROL_PORT = str(TCP_CONTROL_PORT)
        self.USE_PORT = str(TCP_USE_PORT)
        self.ADDR = "(" + self.CONTROL_IP + "," + self.CONTROL_PORT +")"
        self.STATE = dicts.state['non']
        self.START = "Request:" + " " + self.ADDR + " " 
    def open2_receive(self):
        #Request: (address) (outside) open port 5103 to receive
        self.STATE = dicts.state['active']
        return self.START+"open port"+" "+self.USE_PORT+" "+"to"+" "+"receive"
    def open2_transmit(self):
        #Request: (address) (inside) transmit2 port 5003
        self.STATE = dicts.state['active']     
        return self.START+"transmit2 port"+" "+self.USE_PORT
    def close_receiving(self):
        #Request: (address) (inside) close port 5003 (receiving)
        self.STATE = dicts.state['passive']
        return self.START+"close port"+" "+self.USE_PORT+" "+"(receiving)"
    def close_transmitting(self):
        #Request: (address) (inside) close port 5003 (transmitting)
        self.STATE = dicts.state['passive']
        return self.START+"close port"+" "+self.USE_PORT+" "+"(transmitting)"
