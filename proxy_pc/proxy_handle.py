import os
import sys
import timeit
class Proxy_Handle():
    def __init__(self, dir_name, file_name):
        self.dir_name = dir_name
        self.file_name = file_name
        self.amount = 0
        self.counter = 0
        self.err = 0
    
    def set_amount(self, amount):
        self.amount = amount
    
    def proxy_receiver_only(self, time, amount):
        self.set_amount(amount)
        record = open(os.path.join(self.dir_name, self.file_name), 'a')
        record.write("time: {} |".format(str(time)[0:6])+ " " + "received data: {}".format(self.amount) + " \n")
        record.close()
        
    def proxy_transmitter_only(self, time, amount):
        self.set_amount(amount)
        record = open(os.path.join(self.dir_name, self.file_name), 'a')
        record.write("time: {} |".format(str(time)[0:6])+ " " + "transmitted data: {}".format(self.amount) + " \n")
        record.close()
    
    def get_total(self):
        record = open(os.path.join(self.dir_name, self.file_name), 'r')
        self.counter = self.counter + 1        
        total_data = record.read()
        total_data = total_data.split()
        record.close()
        name = self.dir_name + "/" + self.file_name		
        try:
            total_data = int(total_data[0])
        except IndexError:
            pass
        return total_data
    
    def update_total(self, amount):
        self.set_amount(amount)
        updated_data = self.get_total() + self.amount
        record = open(os.path.join(self.dir_name, self.file_name), 'w')
        record.write('{}'.format(updated_data) + " \n")
        record.close()
    
        
        
        
        
        
    
        
        
        
