from task_finders import *
import os

#This file records initialized variables of emulator as map
time = {'duration': 100, 'delay_interval': 0.1, 'delay_sleep': 0.1}
data = {'size': 100000}
buffer = {'lower': 500 * pow(10, 6), 'upper': 1000 * pow(10, 6)}

def kill_process(task_type, keyword, id, port):
    inst = TaskFinder(task_type, keyword, id)
    inst.get_list()
    pid = inst.get_pid(port)
    os.system('kill {}'.format(pid))

def window_process():
    dir_name = os.getcwd() + '/proxy/'
    os.system('sudo su')
    os.system('sudo modprobe -r tcp_probe')
    os.system('sudo tcp_probe [port=] full=1')
    os.system('sudo chmod 444 /proc/net/tcpprobe')
    os.system(' /proc/net/tcpprobe > {}{} '.format(dir_name, 'window_size.out'))
    
