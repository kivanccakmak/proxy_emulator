#!/usr/bin/env python
from requests import *
from dicts import *
from task_finders import *
from socket_initialize import *
import init
import socket
import sys
import os
import time
import timeit
import dicts
import subprocess
import redis

BUFFER_SIZE = 1024
#size of buffer for messages in between 
#master_controller.py -> slave_controller.py  processes

data_root_port = '5115'
start_dest_port = '5215'
dest_requests = []
    
def master_controller(TCP_IP_ROOT, TCP_PORT_ROOT, TCP_IP_DEST, TCP_PORT_DEST, SOCKET_QUANTITY):
    time.sleep(2)
    subprocess.Popen(["python", "proxy_receiver.py", '192.168.2.45', data_root_port]) 
    #this IP should be readed automatically!!
    time.sleep(2)
    #master proxy starts to receive data from root
    root = tx_socket_initialize(TCP_IP_ROOT, TCP_PORT_ROOT)
    print("socket for proxy receive control is connected")
    dest = tx_socket_initialize(TCP_IP_DEST, TCP_PORT_DEST)
    print("socket for proxy forward control is connected")
    data_dest_ports = get_dest_ports(int(start_dest_port), SOCKET_QUANTITY)
    for i in range(0, SOCKET_QUANTITY): 
        dest_requests.append(Request(TCP_IP_DEST, TCP_PORT_DEST, int(data_dest_ports[i])))
        #station -> (TCP_IP_ROOT, TCP_PORT_ROOT) would transmit data to master
        print "message ->  ", dest_requests[i].open2_receive()
        dest.send(dest_requests[i].open2_receive())
        data = dest.recv(BUFFER_SIZE)    
        print "received:  ", data
    root_request = Request(TCP_IP_ROOT, TCP_PORT_ROOT, int(data_root_port)) 
    print "message -> ", root_request.open2_transmit()
    root.send(root_request.open2_transmit())
    data = root.recv(BUFFER_SIZE)
    print "received: ", data
    for i in range(0, SOCKET_QUANTITY):
        print("dest_requests[i].USE_PORT = {}".format(dest_requests[i].USE_PORT))
        print("subprocess.Popen([python, proxy_transmitter.py, {}, {}])".format(TCP_IP_DEST, dest_requests[i].USE_PORT))
        subprocess.Popen(["python", "proxy_transmitter.py", TCP_IP_DEST, str(dest_requests[i].USE_PORT)])
        print("proxy transmitter is opened from PORT: {}".format(dest_requests[i].USE_PORT))
    time.sleep(50)
    #NOW KILLING ALL PROCESSES
    init.kill_process('master_task', 'proxy_receiver.py', 'proxy', str(data_root_port))
    for i in range(0, SOCKET_QUANTITY):
        dest.send(dest_requests[i].close_receiving())
        data = dest.recv(BUFFER_SIZE)
	print "received:  ", data
        init.kill_process('master_task', 'proxy_transmitter', 'proxy', str(dest_requests[i].USE_PORT))
    dest.close()
    root.close()

def get_dest_ports(starting_port, port_quantity):
    data_dest_ports = []
    data_dest_ports.append(str(starting_port))
    if port_quantity > 1:
        for i in range(1, port_quantity):
	    data_dest_ports.append(str(starting_port + i))
    return data_dest_ports
    
def record_initialize(dir_name):
    try:
        os.mkdir(dir_name)
    except OSError:
        pass
            
def main():
    if len(sys.argv) != 6:
        print("Usage: {} <ROOT IP address> <ROOT PORT NUMBER> <DEST IP address> <DEST PORT NUMBER> <SOCKET QUANTITY>".format(sys.argv[0]))
        sys.exit(1)
    if sys.argv[5] < 1:
	print("<SOCKET QUANTITY> should be greater integer than 0")
        sys.exit(1)
    subprocess.Popen(["redis-server"])
    data  = redis.StrictRedis(host='localhost', port=6379, db=0)
    data.set('rcvd', 0)
    data.set('fwd', 0)
    TCP_IP_ROOT = sys.argv[1]   #IP address of data generator(root)
    TCP_PORT_ROOT = sys.argv[2] #Control Port to communicate with root
    TCP_IP_DEST = sys.argv[3]   #IP address of destination
    TCP_PORT_DEST = sys.argv[4]
    SOCKET_QUANTITY = sys.argv[5]
    record_initialize('proxy')
    master_controller(TCP_IP_ROOT, int(TCP_PORT_ROOT), TCP_IP_DEST, int(TCP_PORT_DEST), int(SOCKET_QUANTITY))
    
if __name__ == "__main__":
    main()
