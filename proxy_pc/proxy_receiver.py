from proxy_handle import *
import socket
import sys
import os
import time
import timeit
import dicts
import init
import redis

data  = redis.StrictRedis(host='localhost', port=6379, db=0) 
#map that records forwarded and received data to proxy 
#all proxy_receiver.py and proxy_transmitter.py processes can get values and update values 

duration = init.time['duration']; delay_interval = init.time['delay_interval'] 
#emulation time parameters in seconds

data_size = init.data['size']
#minimum amount of data that would be send from socket

def proxy_receiver(TCP_IP, TCP_PORT, file_name, dir_name):
    delay = 0; proxy_buffer = 0; time = 0;
    buf = bytearray(data_size)
    record = Proxy_Handle(dir_name, file_name)
    #to seperately record output of this socket into file_name 
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((TCP_IP, TCP_PORT))
    s.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
    s.listen(1)
    print("Proxy is ready to receive from PORT: {} ".format(TCP_PORT))
    conn, addr = s.accept()
    print("Proxy receiver accepted connection from {}".format(addr))
    tic = timeit.default_timer()
    while(time < duration):
        time = float(timeit.default_timer() - tic)
        amount = conn.recv_into(buf)
        data.set('rcvd', int(data.get('rcvd'))+amount)
        record.proxy_receiver_only(time, amount)
        proxy_buffer = int(data.get('rcvd')) - int(data.get('fwd'))
        decision = get_decision(delay, proxy_buffer, init.buffer['upper'], init.buffer['lower'])
        if decision == dicts.delay['increase']:
            delay = delay + delay_interval
            os.system('sleep {}'.format(delay))
        elif decision == dicts.delay['decrease']:
            delay = delay - delay_interval 
            os.system('sleep {}'.format(delay))
    conn.close()

def get_decision(delay, proxy_buffer, upper_limit, lower_limit):
    decision = dicts.delay['non']#STEP 1 -> INITIALIZING
    if (proxy_buffer < lower_limit):   #STEP 2 -> BUFFER MANAGEMENT
        if (delay > 0):
            decision = dicts.delay['decrease']
    elif (proxy_buffer > upper_limit):
            decision = dicts.delay['increase']
    return decision

def main():
    if len(sys.argv) != 3:
        print("Usage: {} <IP address> <PORT NUMBER>".format(sys.argv[0]))
        sys.exit(1)
    TCP_IP = sys.argv[1]
    TCP_PORT = int(sys.argv[2])
    file_name = "proxy_receiver_"+sys.argv[2]
    dir_name = 'proxy'
    proxy_receiver(TCP_IP, TCP_PORT, file_name, dir_name)
    
if __name__ == "__main__":
    main()    
