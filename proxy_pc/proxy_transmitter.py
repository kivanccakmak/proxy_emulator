from socket_initialize import *
from proxy_handle import *
import socket
import sys
import os
import time
import timeit
import dicts
import init
import errno
import redis

data  = redis.StrictRedis(host='localhost', port=6379, db=0) 
#map that records forwarded and received data to proxy 
#all proxy_receiver.py and proxy_transmitter.py processes can get values and update values 

duration = init.time['duration']; delay_interval = init.time['delay_interval'] 
#emulation time parameters in seconds

data_size = init.data['size']
#minimum amount of data that would be send from socket

def proxy_transmitter(TCP_IP, TCP_PORT, file_name, dir_name):
    print("TCP_PORT = {}".format(TCP_PORT))
    TCP_PORT = int(TCP_PORT)
    delay = 0; time = 0;
    record = Proxy_Handle(dir_name, file_name)
    #to seperately record output of this socket into file_name 
    s = tx_socket_initialize(TCP_IP, TCP_PORT)
    print("Socket transmitter is connected")
    tic = timeit.default_timer()
    buf = memoryview(bytearray(data_size))
    while(time < duration):
        tot_send_bytes = 0
        time = float(timeit.default_timer() - tic)
        if(int(data.get('rcvd')) - int(data.get('fwd')) > data_size):
            try:
                while tot_send_bytes < init.data['size']:
                    amount = s.send(buf[tot_send_bytes:])
                    tot_send_bytes += amount
                    data.set('fwd', int(data.get('fwd')) + amount)
                    record.proxy_transmitter_only(time, amount)
            except socket.error:
                print("*** socket is closed ***") 
                #receiver socket is closed
                break
        else:
            os.system('sleep {}'.format(init.time['delay_sleep']))
    s.close()

def main():
    if len(sys.argv) != 3:
        print("Usage: {} <IP address> <PORT NUMBER> ".format(sys.argv[0]))
        sys.exit(1)
    TCP_IP = sys.argv[1]
    TCP_PORT = sys.argv[2]
    file_name = "proxy_transmitter_"+sys.argv[2]
    proxy_transmitter(TCP_IP, TCP_PORT, file_name, 'proxy')
    
if __name__ == "__main__":
    main()    
